/**
 * Created by instancetype on 6/8/14.
 */
var connect = require('connect');
var app = connect();

app.use(logger);
app.use('/admin', restrict);
app.use('/admin', admin);
app.use(hello);
app.listen(3000);



function logger(req, res, next) {
    console.log('%s %s', req.method, req.url);
    next();
}

function hello(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello\n');
}

function restrict(req, res, next) {
    var authorization = req.headers.authorization;
    if (!authorization) return next(new Error('Unauthorized'));

    var parts = authorization.split(' '),
        scheme = parts[0],
        auth = new Buffer(parts[1], 'base64').toString().split(':'),
        user = auth[0],
        pass = auth[1];

    authenticateWithDatabase(user, pass, function(err) {
        if (err) return next(err);

        next();
    });
}

function admin(req, res, next) {
    switch (req.url) {

        case '/':
            res.end('try /users');
            break;

        case '/users':
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(['tobi', 'loki', 'jane']));
            break;
    }
}

function authenticateWithDatabase(user, pass, cb) {
    dbStub = [
        { 'name' : 'username',
          'password' : 'password'
        }
    ];

    dbStub.forEach(function(item) {
        if (item.name === user && item.password === pass) return cb();
    });

    return cb(new Error("Authentication failed"));
}
